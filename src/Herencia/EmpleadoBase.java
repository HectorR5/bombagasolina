/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Hector Ramirez
 */
public class EmpleadoBase extends Empleado implements Impuesto{
    
    private float pagoDia;
    private float diasTrabajados;

    public EmpleadoBase() {
        this.diasTrabajados = 0.0f;
        this.pagoDia = 0.0f;
    }

    public EmpleadoBase(float pagoDia, float diasTrabajados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    @Override
    public float calcularPago() {
            return this.pagoDia * this.diasTrabajados;
    }

    @Override
    public float calcularImpuesto() {
        float impuestos = 0.0f;
        if(this.calcularPago()>5000) impuestos=this.calcularPago()*.16f;
        return impuestos;
    }
    
}
